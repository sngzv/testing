import lombok.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class Human {
    private String name;
    private String surname;
    private String patronymic;
    private int age;
    private Sex sex;
    private LocalDate date;

    private static List<Human> people;

    public Human(String name, String surname, String patronymic, int age, Sex sex, LocalDate date) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.age = age;
        this.sex = sex;
        this.date = date;

        if (people == null) {
            people = new ArrayList<>();
        }

        people.add(this);
    }

    public static List<Human> getPeople() {
        return new ArrayList<>(people);
    }

    @Override
    public String toString() {
        return  name + " " +
                surname + " " +
                patronymic + " " +
                age + " " +
                sex + " " +
                date;
    }
}
