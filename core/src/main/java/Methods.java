import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

public class Methods implements Example {

    public void z1(Human person) {
        Human.getPeople().forEach(human -> {
            if (human.equals(person)) {
                System.out.println(person + ": совпадение есть.");
            }
        });
    }

    public void z2() {
        String s = "абвгд";
        for (Human human : Human.getPeople()) {
            if (human.getAge() < 20 && s.contains(String.valueOf(human.getName().toLowerCase().charAt(0)))) {
                System.out.println(human);
            }
        }
    }

    public String z3(Human human) {
        return Collections.frequency(Human.getPeople(), human) + ":" + human;
    }
}