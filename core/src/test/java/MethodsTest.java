import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MethodsTest {

    @Test
    public void testZ3Method() {
        Human dima = new Human("Dima", "Sineguzov", "Vladimirovihc", 20, Sex.MALE, LocalDate.of(2000, 2, 21));
        Human dima1 = new Human("Dima", "Sineguzov", "Vladimirovihc", 20, Sex.MALE, LocalDate.of(2000, 2, 21));
        Human dima2 = new Human("Dima", "Sineguzov", "Vladimirovihc", 20, Sex.MALE, LocalDate.of(2000, 2, 21));
        Human masha = new Human("Masha", "Semenova", "Petrovna", 22, Sex.FEMALE, LocalDate.of(1999, 3, 11));
        Human misha = new Human("Misha", "Petrov", "Ivanovich", 62, Sex.MALE, LocalDate.of(1956, 6, 16));

        Assert.assertEquals("3:Dima Sineguzov Vladimirovihc 20 MALE 2000-02-21", new Methods().z3(dima));

    }

}
